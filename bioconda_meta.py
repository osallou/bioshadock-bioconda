import os
import sys
import json
from subprocess import check_output

package_name = sys.argv[1]

out = check_output(["conda", "install", package_name])


for root, dirs, filenames in os.walk('/opt/conda/conda-meta/'):
    for f in filenames:
        if f.startswith(package_name+'-'):
            with open(os.path.join(root, f)) as data_file:
                meta = json.load(data_file)
            files = meta['files']
            bin_files = []
            for bin_file in files:
                if bin_file.startswith('bin/'):
                    bin_files.append(bin_file.replace('bin/', ''))
            files_file = open('/docker/files.json', 'w')
            files_file.write(json.dumps(bin_files))
            files_file.close()
