import git
import os
import yaml
import logging
import requests
import json
import base64
from subprocess import check_output

logging.basicConfig(level=logging.DEBUG)

def update_containers(metas):
    for meta in metas:
        meta_data = None
        meta_labels = {}
        try:
            with open(meta, 'r') as stream:
                meta_data = yaml.load(stream)
            for major in list(meta_data.keys()):
                if meta_data[major] is None:
                    continue
                for minor in list(meta_data[major].keys()):
                    meta_labels['bioconda.'+major+'.'+minor.title()] = meta_data[major][minor]
                    meta_data['bioshadock_tests'] = None
                    if major == 'test' and minor == 'commands':
                        meta_labels['bioshadock.tests'] = base64.b64encode(json.dumps(meta_data[major][minor]))
            meta_name = meta_data['package']['name']
            if len(meta_name.split(' ')) > 1:
                meta_name = meta_name.split(' ')[0]
            package_name = meta_name.replace(".","-")
            docker_dir = os.path.join(cfg['docker_dir'], 'bioconda', package_name)
            if not os.path.exists(docker_dir):
                os.makedirs(docker_dir)
            if os.path.exists(os.path.join(docker_dir, 'Dockerfile')):
                os.remove(os.path.join(docker_dir, 'Dockerfile'))

            if cfg['docker']['use']:
                # Extract list of binaries from package
                if cfg['docker']['host'] is not None:
                    check_output(["docker", "-H", cfg['docker']['host'], "run", "-v", os.path.dirname(os.path.realpath(__file__)) +":/opt/bioshadock", "-v", docker_dir+":/docker:rw", "--rm", "osallou/bioconda", "python", "/opt/bioshadock/bioconda_meta.py", meta_name])
                else:
                    check_output(["docker", "run", "-v", os.path.dirname(os.path.realpath(__file__)) +":/opt/bioshadock", "-v", docker_dir+":/docker:rw", "--rm", "osallou/bioconda", "python", "/opt/bioshadock/bioconda_meta.py", meta_name])

            dockerfile = ''
            dockerfile += 'FROM '+cfg['docker_from']+'\n'
            dockerfile += 'MAINTAINER '+cfg['maintainer']+'\n'
            dockerfile += 'LABEL '
            for label in list(meta_labels.keys()):
                dockerfile += '    '+label + '="' + str(meta_labels[label]).replace("\"","")+'" \\ \n'
                if os.path.exists(os.path.join(docker_dir,'files.json')):
                    with open(os.path.join(docker_dir, 'files.json')) as f_files:
                        json_files = json.load(f_files)
                        binaries = ",".join(json_files)
                        dockerfile += '    bioconda.binaries="'+binaries+'" \\ \n'
            dockerfile += '    bioshadock.Vendor="bioshadock"\n'
            dockerfile += 'RUN conda install -y '+meta_name+' || conda install -y python=2 '+meta_name+' || conda install -y python=3 '+meta_name+'\n'
            orig_b64 = None
            if os.path.exists(os.path.join(docker_dir, 'Dockerfile')):
                with open(os.path.join(docker_dir, 'Dockerfile'), 'r') as content_file:
                    dockerfile_orig_content = content_file.read()
                    orig_b64 = base64.b64encode(dockerfile_orig_content)
            new_b64 = base64.b64encode(dockerfile)
            if orig_b64 is None or orig_b64 != new_b64:
                with open(os.path.join(docker_dir, 'Dockerfile'), 'w') as f:
                    f.write(dockerfile)
                with open(os.path.join(docker_dir, 'docker.new'), 'w') as stream:
                    stream.write('to update')
                with open(os.path.join(docker_dir, 'package.yaml'), 'w') as f:
                    f.write('package:\n')
                    f.write('    name: \''+meta_name+'\'\n')
                    f.write('    container: \'bioconda/'+meta_name.replace('.', '-')+'\'\n')
                    if 'version' in meta_data['package']:
                        f.write('    version: \''+str(meta_data['package']['version'])+'\'\n')
                    summary = meta_name
                    if 'about' in meta_data and 'summary' in meta_data['about']:
                        summary = meta_data['about']['summary'].replace("'","")
                    f.write('about:\n')
                    f.write('    summary: \''+summary+'\'\n')
                logging.warn("Generated "+meta_name)
            else:
                logging.info("Same package " + meta_name + ", skipping")
                continue
        except Exception as e:
            logging.error('Error occured on '+str(meta)+': '+str(e))


cfg = None

with open("config.yml", 'r') as stream:
    try:
        cfg = yaml.load(stream)
    except yaml.YAMLError as exc:
        logging.error(exc)

repo_dir = os.path.join(cfg['work_dir'], 'biocondarepo')
if not os.path.exists(repo_dir):
    repo = git.Repo.clone_from('https://github.com/bioconda/bioconda-recipes.git', repo_dir, branch='master')
else:
    repo = git.Repo(repo_dir)

head = repo.head

# last_commit_file = os.path.join(cfg['work_dir'], 'bioconda_last_commit')

last_commit = None

# if os.path.exists(last_commit_file):
#     with open(last_commit_file) as f:
#         last_commit = f.read().replace("\n", "")

if last_commit is None:
    logging.warn('New commit, updating...')

    updated_files = []

    dirs = os.listdir(os.path.join(repo_dir,'recipes'))
    for condadir in dirs:
        if os.path.isdir(os.path.join(repo_dir,'recipes', condadir)):
            if os.path.exists(os.path.join(repo_dir,'recipes', condadir,'meta.yaml')):
                condafile = os.path.join(repo_dir,'recipes', condadir,'meta.yaml')
                logging.debug('Meta: '+condafile)
                updated_files.append(condafile)

    logging.debug(str(updated_files))
    update_containers(updated_files)
