FROM osallou/bioconda-base

MAINTAINER Olivier Sallou <olivier.sallou@irisa.fr>


RUN ["conda", "config", "--add", "channels", "r"]
RUN ["conda", "config", "--add", "channels", "bioconda"]
